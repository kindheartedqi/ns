
# 切换镜像源

# 安装命令

npm i qqns -g
##### qqns list 星号为当前源

* npm-------  https://registry.npmjs.org/
  
  yarn------  https://registry.yarnpkg.com/

  tencent---  https://mirrors.cloud.tencent.com/npm/

  cnpm------  https://r.cnpmjs.org/

  taobao----  https://registry.npmmirror.com/
  
  npmMirror-  https://skimdb.npmjs.com/registry/

##### qqns change 切换源

选择你要切换的源

##### qqns current 查看当前源

当前源: npm

##### qqns add 添加源

1.输入添加的名称
2.输入源地址

##### qqns ping 测试源

? 请选择镜像 cnpm
响应时长: 1635ms

##### qqns delete 删除自定义源

add添加的源都可以删除


##### qqns rename 重命名

自定义添加的源都可以进行重新命名

##### qqns edit 编辑自定义镜像地址

# 用法 Usage

Usage: qqns [options] [command]

Options:

  -V, --version   output the version number

  -h, --help      display help for command


Commands:
  list            查看镜像

  change          请选择镜像

  current         查看当前源

  ping            测试镜像地址速度

  add             自定义镜像

  delete          删除自定义的源

  rename          重命名

  edit            编辑自定义镜像地址

  help [command]  display help for command


### 贡献者

[琦琦](https://gitee.com/kindheartedqi)